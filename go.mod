module gitlab.com/anwski/crude-go-actors

go 1.18

require go.elastic.co/ecszap v1.0.1-0.20210922110956-698ab8c60e81

require (
	github.com/eclipse/paho.mqtt.golang v1.4.1
	github.com/google/uuid v1.3.0
)

require (
	github.com/gorilla/websocket v1.4.2 // indirect
	go.uber.org/multierr v1.11.0 // indirect
	go.uber.org/zap v1.27.0 // indirect
	golang.org/x/net v0.1.0 // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c // indirect
)
